/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author jaiminlakhani
 */
public class AllergyWristband extends BasicWristband {
    String allergy;
    
    AllergyWristband(String name, String dob, String familyDoctor, String allergy) {
        super(name, dob, familyDoctor);
        this.allergy = allergy;
    }
    
    public String getAllergy() {
        return allergy;
    }
}
