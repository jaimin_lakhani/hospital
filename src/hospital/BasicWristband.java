/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author jaiminlakhani
 */
public class BasicWristband {
    String name;
    String dob;
    String familyDoctor;
    
    BasicWristband(String name, String dob, String familyDoctor) {
        this.name = name;
        this.dob = dob;
        this.familyDoctor = familyDoctor;
    }
   
    
    public String getName() {
        return name;
    }
    
    public String getDOB() {
        return dob;
    }
    
    public String getFamilyDoctor() {
        return familyDoctor;
    }
}
