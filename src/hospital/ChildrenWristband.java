/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author jaiminlakhani
 */
public class ChildrenWristband extends BasicWristband{
    String parentName;
    
    ChildrenWristband(String name, String dob, String familyDoctor, String parentName) {
        super(name, dob, familyDoctor);
        this.parentName = parentName;
    }
    
    public String getParentName() {
        return parentName;
    }
}
