/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package hospital;
import java.util.Random;
/**
 *
 * @author jaiminlakhani
 */
public class Hospital {

    /**
     * @param args the command line arguments
     */
    
    public static int Barcode(){
    Random rand = new Random(); //instance of random class
        //generate random values from 0-24
      int int_random = rand.nextInt(999999999);
      return int_random;
    }
    
    public static void main(String[] args) {
        BasicWristband bwb = new BasicWristband("John", "12-19-1984", "Dr. Paul");
        AllergyWristband awb = new AllergyWristband("John","12-19-1984","Dr. Paul","Fexofenadine");
        ChildrenWristband cwb = new ChildrenWristband("Luke","02-17-2013","Dr. Paul","John");
        
        System.out.println("Basic Wrist Band Detail");
        System.out.println("Barcode Number: "+Barcode());
        System.out.println("Name: "+bwb.getName());
        System.out.println("Date of Birth: "+bwb.getDOB());
        System.out.println("Family Doctor: "+bwb.getFamilyDoctor()+"\n");
        
        System.out.println("Allergy Wrist Band Detail");
        System.out.println("Barcode Number: "+Barcode());
        System.out.println("Name: "+awb.getName());
        System.out.println("Date of Birth: "+awb.getDOB());
        System.out.println("Family Doctor: "+awb.getFamilyDoctor());
        System.out.println("Medication Allergy: "+awb.getAllergy()+"\n");
        
        System.out.println("Children Wrist Band Detail");
        System.out.println("Barcode Number: "+Barcode());
        System.out.println("Name: "+cwb.getName());
        System.out.println("Date of Birth: "+cwb.getDOB());
        System.out.println("Family Doctor: "+cwb.getFamilyDoctor());
        System.out.println("Parent Name: "+cwb.getParentName());
    }
    
}
