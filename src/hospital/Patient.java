/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author jaiminlakhani
 */
public class Patient {
    private String name;
    private String dob;
    private String familyDoctor;
    
    public Patient() {}
    
    public Patient(String name, String dob, String familyDoctor) {
        this.name = name;
        this.dob = dob;
        this.familyDoctor = familyDoctor;
    }
    
    public String getName() {
        return name;
    }
    
    public String getDob() {
        return dob;
    }
    
    public String getFamilyDoctor() {
        return familyDoctor;
    }
}
