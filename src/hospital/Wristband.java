/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hospital;

/**
 *
 * @author jaiminlakhani
 */
public class Wristband {
    private String barCode;
    private String informationSection;
    
    public Wristband(String barcode, String informationSection) {
        this.barCode = barcode;
        this.informationSection = informationSection;
    }
    
    public String getBarcode() {
        return barCode;
    }
    
    public String getInformationSection() {
        return informationSection;
    }
}
